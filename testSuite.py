import unittest
from testBirthDate import TestBirthDate
from testLegality import TestLegality
from testAge import TestAge
from testPerson import TestPerson
from mockTest import TestMockPerson
from mockTest import TestMockBirthDate
from testUtils import TestUtils

def suiteWithAll():

    test_suite = unittest.TestSuite()
    test_suite.addTest(unittest.makeSuite(TestBirthDate))
    test_suite.addTest(unittest.makeSuite(TestAge))
    test_suite.addTest(unittest.makeSuite(TestLegality))
    test_suite.addTest(unittest.makeSuite(TestPerson))
    test_suite.addTest(unittest.makeSuite(TestMockPerson))
    test_suite.addTest(unittest.makeSuite(TestMockBirthDate))
    test_suite.addTest(unittest.makeSuite(TestUtils))

    return test_suite

mySuit = suiteWithAll()

runner = unittest.TextTestRunner()
runner.run(mySuit)