import unittest
from person import Person
from birthDate import BirthDate
from datetime import datetime
from timeoutDecorator import timeout

class TestAge(unittest.TestCase):
    def setUp(self):
        birthdate2 = BirthDate("16-11-1996")
        self.person = Person("Viorel", "Florea", birthdate2)

    def tearDown(self):
        pass

    # performance

    @timeout(1)
    def testAge(self):
        expected_age = 22
        calculated_age = self.person.getAge()
        self.assertEquals(expected_age, calculated_age)

    #cross checking

    def testAgeCalculation(self):
        birthdate = BirthDate("15-11-1996")
        calculated_age = birthdate.calculateAge()
        days_in_year = 365.2425
        expected_age = int((datetime.now() - datetime.strptime("15-11-1996","%d-%m-%Y")).days / days_in_year)
        self.assertEquals(calculated_age,expected_age)
