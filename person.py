class Person:

    __first_name = []
    __last_name = []

    def __init__(self, first_name, last_name, birthdate):
        self.setFirstName(first_name)
        self.setLastName(last_name)
        self.__birthdate = birthdate

    def setFirstName(self, user_name):
        self.__first_name = []
        names = user_name.split(' ')
        for name in names:
            name = name.capitalize()
            self.__first_name.append(name)

    def getFirstName(self):
        return self.__first_name

    def getFirstNameString(self):
        name = ""
        if len(self.__first_name) > 0:
            for fn in self.__first_name:
                name = name + fn + " "
            name = name[:-1]
        return name

    def setLastName(self,user_name):
        self.__last_name = []
        names = user_name.split(' ')
        for name in names:
            name = name.capitalize()
            self.__last_name.append(name)

    def getLastName(self):
        return self.__last_name

    def getLastNameString(self):
        name = ""
        if len(self.__last_name) > 0:
            for fn in self.__last_name:
                name = name + fn + " "
            name = name[:-1]
        return name

    def setBirthdate(self, birthdate):
        self.__birthdate = birthdate

    def getBirthdateString(self):
        return self.__birthdate.getBirthDateString()

    def getBirthdate(self):
        return self.__birthdate.getBirthDateObject()

    def getFullName(self):
        return self.getFirstNameString() + " " + self.getLastNameString()

    def getAge(self):
        return self.__birthdate.calculateAge()

    def hasLegalAge(self):
        try:
            age = self.__birthdate.calculateAge()
            if age >= 18:
                return True
            return False
        except ValueError:
            raise ValueError("Birthday missing")
