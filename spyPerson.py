from birthDate import BirthDate

class PersonSpy:
    __first_name = []
    __last_name = []
    numberGetAge = 0
    numberGetFirstName = 0
    numberGetLastName = 0
    numberGetBirthDate = 0

    def __init__(self, first_name = "", last_name = "", age = 0):
        self.setFirstName(first_name)
        self.setLastName(last_name)
        self.__birthdate = None
        self.__age = age

    def setFirstName(self, user_name):
        self.__first_name = []
        names = user_name.split(' ')
        for name in names:
            self.__first_name.append(name)

    def getFirstName(self):
        self.numberGetFirstName = self.numberGetFirstName + 1
        print self.numberGetFirstName
        return ["Viorel"]

    def getFirstNameString(self):
        self.numberGetFirstName = self.numberGetFirstName + 1
        print self.numberGetFirstName
        return "Viorel"

    def setLastName(self, user_name):
        self.__last_name = []
        names = user_name.split(' ')
        for name in names:
            self.__last_name.append(name)

    def getLastName(self):
        self.numberGetLastName = self.numberGetLastName + 1
        print self.numberGetLastName
        return ["Florea"]

    def getLastNameString(self):
        self.numberGetLastName = self.numberGetLastName + 1
        print self.numberGetLastName
        return "Florea"

    def setBirthdate(self, birthdate):
        self.__birthdate = birthdate

    def getBirthdateString(self):
        self.numberGetBirthDate = self.numberGetBirthDate + 1
        print self.numberGetBirthDate
        return "15-11-1996"

    def getBirthdate(self):
        self.numberGetBirthDate = self.numberGetBirthDate + 1
        print self.numberGetBirthDate
        return BirthDate("15-11-1996")

    def getFullName(self):
        return "Viorel Florea"

    def getAge(self):
        self.numberGetAge = self.numberGetAge + 1
        print self.numberGetAge
        return 22

    def hasLegalAge(self):
        return True
