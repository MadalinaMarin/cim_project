from birthDate import BirthDate

class PersonStub:
    __first_name = []
    __last_name = []

    def __init__(self, first_name = "", last_name = "" , age = 0):
        self.setFirstName(first_name)
        self.setLastName(last_name)
        self.__birthdate = None
        self.__age = age

    def setFirstName(self, user_name):
        self.__first_name = []
        names = user_name.split(' ')
        for name in names:
            self.__first_name.append(name)

    def getFirstName(self):
        return ["Viorel"]

    def getFirstNameString(self):
        return "Viorel"

    def setLastName(self, user_name):
        self.__last_name = []
        names = user_name.split(' ')
        for name in names:
            self.__last_name.append(name)

    def getLastName(self):
        return ["Florea"]

    def getLastNameString(self):
        return "Florea"

    def setBirthdate(self, birthdate):
        self.__birthdate = birthdate

    def getBirthdateString(self):
        return "15-11-1996"

    def getBirthdate(self):
        return BirthDate("15-11-1996")

    def getFullName(self):
        return "Viorel Florea"

    def getAge(self):
        return 22

    def hasLegalAge(self):
        return True