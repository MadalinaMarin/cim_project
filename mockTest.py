from unittest import TestCase
from mock import patch
from person import Person
from birthDate import BirthDate
from utils import calculateAgeFromDate
from utils import funnyNaming
import datetime

class TestMockPerson(TestCase):
    @patch('person.Person')
    def testFunnyNaming(self, person):
        person.getFirstNameString.return_value = 'Viorel'
        person.getLastNameString.return_value = "Florea"
        person.getFullName.return_value = "Viorel Florea"
        self.assertEquals(funnyNaming(person.getFullName()), "aerolF leroiV")

class TestMockBirthDate(TestCase):
    @patch('birthDate.BirthDate')
    def testAge(self, birthdate):
        birthdate.getBirthDateString.return_value = "15-11-1996"
        birthdate.getBirthDateDateTime.return_value = datetime.datetime.strptime("15-11-1996", "%d-%m-%Y")
        self.assertEquals(calculateAgeFromDate(birthdate.getBirthDateDateTime()), 22)
