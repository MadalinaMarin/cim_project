import unittest
from utils import calculateAgeFromDate, funnyNaming, calculateTimestamp
from birthDate import BirthDate
from person import Person
from datetime import date,datetime
from personStub import PersonStub

class TestUtils(unittest.TestCase):
    def setUp(self):
        self.birthdate = BirthDate("15-11-1996")
        self.person = Person("Viorel", "Florea", self.birthdate)

    def tearDown(self):
        pass

#test inverse

    def testInverseFunnyName(self):
        first_result = funnyNaming(self.person.getFullName())
        second_result = funnyNaming(first_result)
        self.assertEquals(self.person.getFullName(), second_result)

    def testInverseMultipleFunnyName(self):
        person = Person("Viorel", "Florea", self.birthdate)
        person.setFirstName("Ceva Ceva Ceva Ceva")
        first_result = funnyNaming(self.person.getFullName())
        second_result = funnyNaming(first_result)
        self.assertEquals(self.person.getFullName(), second_result)

    def testFunnyNameNone(self):
        try:
            name = funnyNaming(None)
        except ValueError:
            print(ValueError.message)

    def testFunnyName(self):
        name = funnyNaming("Anapa")
        self.assertEquals(name,"apanA")

    def testFunnyNamingErrorValue(self):
        person = Person("", "", self.birthdate)
        try:
            funnyNaming(person.getFirstNameString())
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)

    def testCalculateTimestamp(self):
        value = date(2010, 1, 1).strftime("%d-%m-%Y")
        self.assertEquals(calculateTimestamp(value), 1262296800)

    def testCalculateTimestamp(self):
        value = date(2019, 7, 11).strftime("%d-%m-%Y")
        self.assertAlmostEqual(calculateTimestamp(value), 1562792400, 1)

    def testCalculateAge(self):
        self.assertEquals(calculateAgeFromDate(datetime.today()), 0)

    # stub

    def testStubFunnyPersonLastName(self):
        person = PersonStub()
        self.assertEquals(  funnyNaming(person.getFirstNameString()), "leroiV")

    def testStubPersonBirthDay(self):
        person = PersonStub()
        self.assertEquals(person.getBirthdateString(), "15-11-1996")