import datetime
import time

def funnyNaming(name):
    if name == None or name == "":
        raise ValueError
    return ''.join(reversed(name))

def calculateAgeFromDate(birthdate):
    if birthdate == None:
        raise ValueError("Birthdate cannot be None")

    today = datetime.datetime.now()
    try:
        birthday = birthdate.replace(year = today.year)

    # raised when birth date is February 29
    # and the current year is not a leap year
    except ValueError:
        birthday = birthdate.replace(year = today.year,
                                            month = birthdate.month + 1, day=1)
        raise

    if birthday > today:
        return today.year - birthdate.year - 1
    else:
        return today.year - birthdate.year

def calculateMonthFromDate(birthdate):
    if birthdate == None:
        raise ValueError("Birthdate cannot be None")

    today = datetime.datetime.now()
    try:
        birthday = birthdate.replace(year=today.year)

    # raised when birth date is February 29
    # and the current year is not a leap year
    except ValueError:
        birthday = birthdate.replace(year=today.year,
                                     month=birthdate.month + 1, day=1)
        raise


    return today.month - birthdate.month


def calculateTimestamp(date_string):
    return time.mktime(datetime.datetime.strptime(date_string, "%d-%m-%Y").timetuple())
