import unittest
from person import Person
from birthDate import BirthDate
from spyPerson import PersonSpy

class TestPerson(unittest.TestCase):
    def setUp(self):
        self.birthdate = BirthDate("15-11-1996")
        pass

    def tearDown(self):
        pass

    def testFirstNameChange(self):
        person = Person("Viorel", "Florea", self.birthdate)
        person.setFirstName("Gigel")
        expected_first_name = "Gigel"
        first_name = person.getFirstNameString()
        self.assertEquals(expected_first_name,first_name)

    def testFullNameSize(self):
        person = Person("Viorel Ionut", "Florea", self.birthdate)
        self.assertEquals(len(person.getFullName()), len("Viorel Ionut Florea"))

    def testLastNameChange(self):
        birthdate = BirthDate("15-11-1996")
        person = Person("Viorel", "Florea", self.birthdate)
        person.setLastName("Gigel")
        expected_last_name = "Gigel"
        last_name = person.getLastNameString()
        self.assertEquals(expected_last_name,last_name)

    def testMultipleFirstNameChange(self):
        birthdate = BirthDate("15-11-1996")
        person = Person("Viorel Ionut", "Florea", self.birthdate)
        person.setFirstName("Gigel")
        expected_last_name = "Gigel"
        last_name = person.getFirstNameString()
        self.assertEquals(expected_last_name,last_name)

    #conformance

    def testConformanceFirstName(self):
        person = Person("Viorel","Florea", self.birthdate)
        self.assertGreaterEqual(len(person.getFirstName()), 1)

    def testConformanceLastName(self):
        person = Person("Viorel", "Florea", self.birthdate)
        self.assertGreaterEqual(len(person.getLastName()), 1)

    def testFirstNameCapitalize(self):
        person = Person("viorel ionut Andrei","Florea", self.birthdate)
        self.assertEqual(person.getFirstNameString()[0], "V")

    def testFirstNameCapitalize(self):
        person = Person("Viorel","florea", self.birthdate)
        self.assertEqual(person.getLastNameString()[0], "F")

    #ordering

    def testNameOrdering(self):
        person = Person("Viorel", "Florea", self.birthdate)
        full_name = person.getFullName()
        names = full_name.split(' ')
        if names[1] != "Florea" or names[0] != "Viorel":
            self.fail("The order is not correct")

    #range

    def testNameSize(self):
        person = Person("Viorel", "Florea", self.birthdate)
        full_name = person.getFullName()
        names = full_name.split(' ')
        self.assertEquals(len(names),2)

    def testMultipleNamesSize(self):
        person = Person("Viorel Ionut", "Florea", self.birthdate)
        full_name = person.getFullName()
        names = full_name.split(' ')
        self.assertEquals(len(names), 3)

    # existance

    def testBirthDateExistance(self):
        birthdate = BirthDate()
        person = Person("Viorel Ionut", "Florea", birthdate)
        try:
            isLegal = person.hasLegalAge()
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)

    #cardinality

    def test0CardinalityFirstName(self):
        person = Person("","", self.birthdate)
        first_name = person.getFirstNameString()
        self.assertEquals(len(first_name), 0)

    def test1CardinalityFirstName(self):
        person = Person("Viorel", "", self.birthdate)
        first_name = person.getFirstNameString()
        names = first_name.split(' ')
        self.assertEquals(len(names), 1)

    def test1CardinalityFirstName(self):
        person = Person("Viorel Ionut Andrei", "Florea", self.birthdate)
        first_name = person.getFirstNameString()
        names = first_name.split(' ')
        self.assertEquals(len(names), 3)

    #spy

    def testSpyPersonFirstName(self):
        person = PersonSpy()
        self.assertEquals(person.getFirstNameString(), "Viorel")

    def testSpyPersonLastName(self):
        person = PersonSpy()
        self.assertEquals(person.getLastNameString(), "Florea")

    def testSpyPersonBirthDay(self):
        person = PersonSpy()
        self.assertEquals(person.getBirthdateString(), "15-11-1996")

