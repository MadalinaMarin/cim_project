import unittest
from person import Person
from birthDate import BirthDate
from datetime import datetime
import time
from timeoutDecorator import timeout
from datetime import date

class TestBirthDate(unittest.TestCase):
    def setUp(self):
        birthdate2 = BirthDate("16-11-1996")
        self.person = Person("Viorel", "Florea", birthdate2)

    def tearDown(self):
        pass

    def testBirthDateNotNull(self):
        birthdate = BirthDate("15-11-1996")
        self.assertIsNotNone(birthdate.getBirthDateString())

    def testBirthDateNameNull(self):
        birthdate = BirthDate()
        self.assertIsNone(birthdate.getBirthDateString())

#same

    def testIsBirthdayObject(self):
        birthdate = BirthDate("15-11-1996")
        self.assertIs(birthdate, birthdate.getBirthDateObject())

#not same

    def testIsBirthdayObject(self):
        birthdate = BirthDate("15-11-1996")
        self.assertIsNot(birthdate, self.person.getBirthdate())

#ERROR:

    def testBirthdateWrongFormat(self):
        try:
            birthdate = BirthDate("15/11/1996")
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)

    def testBirthdayWrongFormatReverse(self):
        try:
            birthdate = BirthDate("1996-11-15")
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)

    def testBirthdayWithTime(self):
        try:
            birthdate = BirthDate("15/11/1996 12:32")
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)

    def testBirthdayOn29Feb(self):
        try:
            birthday = BirthDate("29-02-2018")
            self.fail("Should fail")
        except ValueError:
            print(ValueError.message)


#cross checking

    @timeout(1)
    def testBirthdayToTimestamp(self):
        birthdate = BirthDate("15-11-1996")
        timestamp_calculated = birthdate.calculateTimestamp()
        dt = datetime(year=1996, month=11, day=15)
        timestamp_expected = time.mktime(dt.timetuple())
        self.assertEquals(timestamp_calculated, timestamp_expected)

#conformance

    def testConformanceBirthdate(self):
        birthdate = BirthDate("15-11-1996").getBirthDateDateTime()
        current_date = datetime.strptime(date.today().strftime("%d-%m-%Y"),"%d-%m-%Y").date()
        self.assertGreaterEqual(current_date, birthdate)

    def testCalculateAgeInFuture(self):
        birthdate = BirthDate("15-11-2020")
        try:
            age = birthdate.calculateAge()
        except ValueError:
            print(ValueError.message)
