import unittest
from person import Person
from birthDate import BirthDate
from datetime import datetime,timedelta
from personStub import PersonStub
from fakePerson import PersonFake
from spyPerson import PersonSpy

class TestLegality(unittest.TestCase):
    def setUp(self):
        self.birthdate = BirthDate("15-11-1996")

    def tearDown(self):
        pass

    # RIGHT

    def testHasLegalAge(self):
        person = Person("Viorel", "Florea", self.birthdate)
        self.assertTrue(person.hasLegalAge())

    def testHasLegalAgeAfterChange(self):
        person = Person("Viorel", "Florea", self.birthdate)
        birthdate = BirthDate("15-11-2010")
        person.setBirthdate(birthdate)
        self.assertFalse(person.hasLegalAge())

    def testHasNotLegalAge(self):
        birthdate = BirthDate("15-11-2006")
        person = Person("Viorel", "Florea", birthdate)
        self.assertFalse(person.hasLegalAge())

    def testBirthdayIsFuture(self):
        dateValue = "15-11-2020"
        birthdate = BirthDate(dateValue)
        person = Person("Viorel", "Florea", birthdate)
        self.assertFalse(person.hasLegalAge())

    # boundary

    def test18thBirthdayIsToday(self):
        dateValue = datetime.now() - timedelta(days=18 * 365.25)
        years_ago_today_string = dateValue.strftime("%d-%m-%Y")
        birthdate = BirthDate(years_ago_today_string)
        person = Person("Viorel","Florea",birthdate)
        self.assertTrue(person.hasLegalAge())

    def testBirthdayIsToday(self):
        dateValue = datetime.now().strftime("%d-%m-%Y")
        birthdate = BirthDate(dateValue)
        person = Person("Viorel", "Florea", birthdate)
        self.assertFalse(person.hasLegalAge())


    # stub

    def testStubPersonLegality(self):
        person = PersonStub("Viorel", "Florea", 22)
        self.assertTrue(person.hasLegalAge())

    # fake

    def testFakePersonLegality(self):
        person = PersonFake("Viorel", "Florea", self.birthdate)
        person.setAge(22)
        self.assertTrue(person.hasLegalAge())

    def testFakePersonLegality(self):
        person = PersonFake("Viorel", "Florea", self.birthdate)
        person.setAge(16)
        self.assertFalse(person.hasLegalAge())

    # spy

    def testSpyPersonAge(self):
        person = PersonSpy("Viorel", "Florea", 22)
        person.getAge()
        person.getAge()
        person.getAge()
        print("Spy person get age: " + str(person.numberGetAge))
        self.assertEquals(person.getAge(), 22)

    def testSpyPerson(self):
        person = PersonSpy()
        self.assertTrue(person.hasLegalAge())

    def testHasLegalAgeMissingBirthday(self):
        birthdate = BirthDate()
        try:
            person = Person("Viorel", "Florea", birthdate)
        except ValueError:
            print(ValueError.message)