import datetime
import time
from utils import calculateAgeFromDate, calculateTimestamp, calculateMonthFromDate
class BirthDate:

    __birthdate = datetime.datetime.now()
    __timestamp = 0

    def __init__(self,birthdate = None):
        try:
            self.__birthdate = self.__convertToDatetime(birthdate)
        except ValueError:
            raise ValueError

    def calculateAge(self):
        return calculateAgeFromDate(self.__birthdate)

    def calculateMonth(self):
        return calculateMonthFromDate(self.__birthdate)

    def getBirthDateString(self):
        if self.__birthdate == None:
            return None
        date_time = self.__birthdate.strftime("%d-%m-%Y")
        return date_time

    def getBirthDateObject(self):
        if self.__birthdate == None:
            return None
        return self

    def getBirthDateDateTime(self):
        return self.__birthdate.date()

    def __convertToDatetime(self, date_string):
        if date_string == None:
            return None
        try:
            return datetime.datetime.strptime(date_string, "%d-%m-%Y")
        except ValueError:
            raise ValueError("Incorrect data format, should be %d-%m-%Y")

    def setBirthDate(self, birthdate):
        if birthdate == None:
            raise Exception("Birthdate cannot be None")
        self.__birthdate = self.convertToDatetime(birthdate)

    def calculateTimestamp(self):
        return calculateTimestamp(self.getBirthDateString())


